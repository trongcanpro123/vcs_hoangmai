<?php

namespace App\Helpers\Assets;


class AdminAsset extends BaseAsset
{

    public $js = [
//        ['https://unpkg.com/react@16/umd/react.development.js', 'crossorigin'],
        ['assets/react/react.development.js', 'crossorigin'],
//        ['https://unpkg.com/react-dom@16/umd/react-dom.development.js', 'crossorigin'],
        ['assets/react/react-dom.development.js', 'crossorigin'],
        'admin/js/jquery.form.min.js',
//        ['https://cdn.bootcss.com/flv.js/1.5.0/flv.min.js','crossorigin'],
        ['assets/flv/flv.min.js','crossorigin'],
        ['assets/bower_components/moment/min/moment.min.js','crossorigin'],
//        ['assets/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js','crossorigin'],
//        ['assets/Date-Time-Picker-Bootstrap-4/build/js/bootstrap-datetimepicker.min.js','crossorigin'],
        ['assets/jquery_datetimepicker/js/jquery.datetimepicker.full.min.js','crossorigin'],
        ['assets/jquery_marquee/jquery.marquee.js','crossorigin'],
        ['assets/sweet_alert/sweet_alert.js','crossorigin'],

        'admin/js/main.js?0.6.6',
        'admin/js/admin_content.js?v=0.0.3',
        'assets/chartjs/Chart.js'
    ];

    public $css = [
//        ['assets/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css','crossorigin'],
//        ['assets/Date-Time-Picker-Bootstrap-4/build/css/bootstrap-datetimepicker.min.css','crossorigin'],

                ['assets/jquery_datetimepicker/css/jquery.datetimepicker.min.css','crossorigin'],

        'admin/css/main.css?v=0.2.4',
    ];

    public $depends = [
        MaterialDashboard::class,
        FroalaEditor::class
    ];
}
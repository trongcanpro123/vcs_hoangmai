<?php


namespace App\Models;
use App\Controllers\Admin\PumpProductType;
use App\Helpers\StringHelper;
use App\Models\Interfaces\ContentInterface;
use App\Models\PumpProductTypeModel;

class TgbxOrderDetailModel extends BaseModel
{
    protected $table = 'tgbx_order_detail';
    protected $primaryKey = 'id';

    protected $useSoftDeletes = false;
    protected $allowedFields = ['ma_lenh', 'so_lenh', 'line_id', 'tong_du_xuat', 'ma_hang_hoa', 'ngay_xuat', 'ma_phuong_tien',
        'nguoi_van_chuyen', 'area_id','pump_system_id','pump_name','throad_id','throad_name','tdh_status','check_time', 'in_out_id', 'order_id'];

    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $dateFormat = 'int';
    /**
     * @param string|null $scenario
     * @return array
     */
    public function getRules(string $scenario = null): array
    {
        return [
        ];
    }
    public function get_ten_hang(){
        $data = $this->db->query('select product_name from pump_product_type where right(product_type_code,6) = ? ',$this->ma_hang_hoa)->getRow();
        if($data){
           return  $data->product_name;
        }
        return '';
    }
    public function create_dynamic_label_table($select_date, $area_id)
    {
        $query_all_label = 'select tgbx_order_detail.ma_hang_hoa, pump_product_type.product_name from tgbx_order_detail inner join pump_product_type
                                     on tgbx_order_detail.ma_hang_hoa = pump_product_type.product_type_code
                                     where DATE_FORMAT(check_time,?) = ? ';
        if ($area_id){
            $query_all_label .= ' AND area_id ='. $area_id;
        }
        $sql_format_date_hour ='%Y-%m-%d';
        $sql_data = $this->db->query($query_all_label,[$sql_format_date_hour, $select_date])->getResult('array');
        return $sql_data;
    }

    public function create_dynamic_label_total_table($from_date, $to_date, $area_id)
    {
        $query_all_label = 'SELECT distinct pump_product_type.product_name, tgbx_order_detail.ma_hang_hoa
                                     FROM tgbx_order_detail 
                                     INNER JOIN pump_product_type
                                     ON tgbx_order_detail.ma_hang_hoa = pump_product_type.product_type_code
                                     WHERE DATE_FORMAT(check_time,?) BETWEEN ? AND ?';
        if ($area_id){
            $query_all_label .= ' AND area_id ='. $area_id;
        }
        $sql_format_date_hour ='%Y-%m-%d';
        $sql_data = $this->db->query($query_all_label,[$sql_format_date_hour, $from_date, $to_date])->getResult('array');
        return $sql_data;
    }

    public function create_dynamic_label_throad_table($from_date, $to_date, $area_id)
    {
        $query_all_label = 'SELECT distinct tgbx_order_detail.throad_name, tgbx_order_detail.throad_id
                                     FROM tgbx_order_detail 
                                     WHERE DATE_FORMAT(check_time,?) BETWEEN ? AND ?';
        if ($area_id){
            $query_all_label .= ' AND area_id ='. $area_id;
        }
        $query_all_label .= ' ORDER BY throad_id';
        $sql_format_date_hour ='%Y-%m-%d';
        $sql_data = $this->db->query($query_all_label,[$sql_format_date_hour, $from_date, $to_date])->getResult('array');
        return $sql_data;
    }

    public function create_dynamic_label_throad_name_table($select_date,$area_id){
        $query_all_label = 'SELECT distinct tgbx_order_detail.throad_name, tgbx_order_detail.throad_id
                                     FROM tgbx_order_detail 
                                     WHERE DATE_FORMAT(check_time,?) = ?';
        if ($area_id){
            $query_all_label .= ' AND area_id ='. $area_id;
        }
        $query_all_label .= ' ORDER BY throad_id';
        $sql_format_date_hour ='%Y-%m-%d';
        $sql_data = $this->db->query($query_all_label,[$sql_format_date_hour, $select_date])->getResult('array');
        return $sql_data;
    }

    public function get_list_throad_wait_time($area_id, $ma_hang_hoa){
        $query = 'SELECT thr.*,hh.`product_type_code`,COALESCE(bx.tong_so_luong,0) AS tong_so_luong,
                    CASE 
                     WHEN thr.wattage = \'0\' THEN 0
                     ELSE (COALESCE(bx.tong_so_luong,0)/thr.`wattage`)
                    END AS wait_time
                     FROM `pump_throad` AS thr 
                    LEFT JOIN 
                    (
                    SELECT `throad_id`, COALESCE(SUM(tong_du_xuat),0) AS tong_so_luong
                    FROM `tgbx_order_detail` AS tgbx WHERE
                    tgbx.`area_id` = ?
                    AND tgbx.`throad_id` IS NOT NULL
                    AND tdh_status =\'dang_cho\'
                    GROUP BY throad_id
                    ) bx ON thr.id = bx.`throad_id`,
                    `pump_product_type` AS hh 
                    
                    WHERE thr.`product_type_id` = hh.`id`
                    AND thr.`area_id` = ?
                    AND thr.`active` =1 
                    AND hh.`product_type_code` = ? ';
        return $this->db->query($query,[$area_id, $area_id, $ma_hang_hoa])->getResultArray();
    }

    public function get_list_throad_wait_time_estimate($area_id, $ma_hang_hoa,$soluong){
        $query = 'SELECT thr.*,hh.`product_type_code`,(COALESCE(bx.tong_so_luong,0) + ?) AS tong_so_luong,
                    CASE 
                     WHEN thr.wattage = \'0\' THEN 0
                     ELSE (COALESCE(bx.tong_so_luong,0) + ? )/thr.`wattage`
                    END AS wait_time
                     FROM `pump_throad` AS thr 
                    LEFT JOIN 
                    (
                    SELECT `throad_id`, COALESCE(SUM(tong_du_xuat),0) AS tong_so_luong
                    FROM `tgbx_order_detail` AS tgbx WHERE
                    tgbx.`area_id` = ?
                    AND tgbx.`throad_id` IS NOT NULL
                    AND tdh_status =\'dang_cho\'
                    GROUP BY throad_id
                    ) bx ON thr.id = bx.`throad_id`,
                    `pump_product_type` AS hh 
                    
                    WHERE thr.`product_type_id` = hh.`id`
                    AND thr.`area_id` = ?
                    AND thr.`active` =1 
                    AND hh.`product_type_code` = ? ';
        return $this->db->query($query,[$soluong, $soluong, $area_id, $area_id, $ma_hang_hoa])->getResultArray();
    }

    public function get_list_throad_pararell_of_order($area_id,$in_out_id){
        $query = 'SELECT thr.*,hh.`product_type_code`,COALESCE(bx.tong_so_luong,0) AS tong_so_luong,
                    CASE 
                     WHEN thr.wattage = \'0\' THEN 0
                     ELSE (COALESCE(bx.tong_so_luong,0)/thr.`wattage`)
                    END AS wait_time
                     FROM `pump_throad` AS thr 
                    LEFT JOIN 
                    (
                    SELECT `throad_id`, COALESCE(SUM(tong_du_xuat),0) AS tong_so_luong
                    FROM `tgbx_order_detail` AS tgbx WHERE
                    tgbx.`area_id` = ?
                    AND tgbx.`throad_id` IS NOT NULL
                    AND tdh_status =\'dang_cho\'
                    GROUP BY throad_id
                    ) bx ON thr.id = bx.`throad_id`,
                    `pump_product_type` AS hh 
                    
                    WHERE thr.`product_type_id` = hh.`id`
                    AND thr.`area_id` = ?
                    AND thr.`active` =1
                    AND hh.`product_type_code` IN 
                    (
                        SELECT DISTINCT `ma_hang_hoa` FROM `tgbx_order_detail` WHERE `in_out_id` = ?
                    ) 
                    AND thread_pararell_id > 0';

        $list_throad = $this->db->query($query,[$area_id, $area_id, $in_out_id])->getResultArray();
        if(!$list_throad || count($list_throad) < 2){
            return false;
        }
        $list_throad_couple = [];
        for($i = 0 ;$i< (count($list_throad) -1) ; $i++){
            for($k = 1; $k< count($list_throad); $k++ ){
                if ($list_throad[$i]['id'] == $list_throad[$k]['thread_pararell_id']){
                    if($list_throad[$i]['wait_time'] > $list_throad[$k]['wait_time']){
                        $list_throad[$i]['calc'] = 1;
                    }else{
                        $list_throad[$k]['calc'] = 1;
                    }
                    $list_throad_couple[] = $list_throad[$i];
                    $list_throad_couple[] = $list_throad[$k];
                }
            }
        }
        if(count($list_throad_couple)== 0){
            return  false;
        }
        return $list_throad_couple;

    }


}
<?php

namespace App\Models;

use App\Helpers\ArrayHelper;
use App\Helpers\SessionHelper;
use App\Helpers\StringHelper;
use App\Models\Interfaces\IdentityInterface;
use Cassandra\Date;
use DateInterval;
use DatePeriod;
use DateTime;

/**
 * Class AdministratorModel
 * @package App\Models
 *
 * @property int $id
 * @property string $account_name
 * @property string $password_hash
 * @property string $full_name
 * @property string $avt
 * @property int $type
 * @property int $is_lock
 *
 * @property int $login_after_init
 */
class AdministratorModel extends BaseModel implements IdentityInterface
{
    protected $table = 'administrator';
    protected $primaryKey = 'id';

    protected $useSoftDeletes = false;

    protected $allowedFields = [
        'full_name', 'account_name', 'password_hash', 'password_salt', 'is_lock', 'login_after_init', 'avt', 'type_user',
        'area_id', 'note'
    ];

    protected $rgb = [
        'rgb(255, 195, 77)',
        'rgb(0, 204, 0)',
        'rgb(153, 153, 100)',
        'rgb(153, 153, 90)',
        'rgb(153, 153, 102)',
        'rgb(44, 24, 69)',
        'rgb(235, 64, 52)',
        'rgb(32, 24, 69)',
        'rgb(255, 99, 132)',
        'rgb(255, 159, 64)',
        'rgb(166, 162, 126)',
        'rgb(255, 205, 86)',
        'rgb(75, 192, 192)',
        'rgb(54, 162, 235)',
        'rgb(153, 102, 255)',
        'rgb(201, 203, 207)',
    ];

    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';
    protected $dateFormat = 'int';

    protected $validationRules = [];
    protected $validationMessages = [];
    protected $skipValidation = false;

    protected $beforeInsert = ['createAvt'];
    protected $beforeUpdate = ['createAvt'];

    /** @var AdministratorModel $_user */
    protected static $_user;

    const LOGIN_KEY = 'PANDA_ADMIN';

    const TYPE_ADMIN = 1;
    const TYPE_MEMBER = 0;


    /**
     * @param array $data
     * @return array
     */
    public function createAvt(array $data): array
    {
        $fullName = ArrayHelper::getValue($data, ['data', 'full_name']);
        if ($fullName && !empty($fullName)) {
            $paths = StringHelper::explode(trim($fullName), ' ');
            $endPath = $paths[count($paths) - 1];
            $data['data']['avt'] = strtoupper($endPath[0]);
        }
        return $data;
    }

    /**
     * @param $password
     * @return string
     */
    public static function createPasswordHash($password): string
    {
        $options = [
            'salt' => 'your_custom_function_for_salt',
            'cost' => 12
        ];
        return password_hash($password, PASSWORD_DEFAULT, $options);
    }

    /**
     * @return bool
     */
    public function login(): bool
    {
        SessionHelper::getInstance()->set(self::LOGIN_KEY, [
            'id' => $this->getPrimaryKey(),
            'account_name' => $this->account_name
        ]);

        return ($data = SessionHelper::getInstance()->get(self::LOGIN_KEY)) !== null && !empty($data);
    }

    /**
     *
     */
    public function logout()
    {
        SessionHelper::getInstance()->remove(self::LOGIN_KEY);
    }

    /**
     * @return null|AdministratorModel|object
     */
    public static function findIdentity()
    {
        if (($data = SessionHelper::getInstance()->get(static::LOGIN_KEY)) === null
            || empty($data) || !($id = ArrayHelper::getValue($data, 'id'))) return null;

        return (new static())->where('is_lock', 0)->find($id);
    }

    public function select_name_area()
    {
        $name_area_db = $this->db->query('SELECT `name` FROM area WHERE id = ?', [$this->area_id])->getRow();
        if ($name_area_db) {
            return $name_area_db->name;
        }
        return '';
    }

    /**
     * @param $password
     * @return bool
     */
    public function validatePasswordHash($password): bool
    {
        if (empty($password)) return false;
        return password_verify($password, $this->password_hash);
    }

    public function select_area_admin_id($id)
    {
        return $this->db->query('SELECT area_id FROM administrator WHERE id = ?', [$id])->getRow();
    }

    /**
     * @param string $accountName
     * @return AdministratorModel|null
     */
    public static function findByUsername($accountName)
    {
        if (static::$_user && static::$_user->account_name === $accountName) return static::$_user;

        /** @var AdministratorModel $model */
        $model = (new static())
            ->where('account_name', trim($accountName))
            ->where('is_lock', 0)
            ->first();

        if (!$model) return null;

        static::$_user = $model;
        return $model;
    }

    /**
     * @param string $scenario
     * @return array
     */
    private function getRulesByScenario(string $scenario): array
    {
        $scenarios = [
            'update' => [
                'full_name' => [
                    'rules' => 'required|min_length[3]|max_length[50]',
                    'errors' => [
                        'required' => 'Họ tên không được để trống',
                        'min_length' => 'Họ tên có ít nhất 3 ký tự',
                        'max_length' => 'Họ tên có nhiều nhất 50 ký tự'
                    ]
                ],
            ]
        ];

        return ArrayHelper::getValue($scenarios, $scenario);
    }

    /**
     * @param string|null $scenario
     * @return array
     */
    public function getRules(string $scenario = null): array
    {
        if ($scenario && ($data = $this->getRulesByScenario($scenario))) {
            return $data;
        }
        return [
            'full_name' => [
                'rules' => 'required|min_length[3]|max_length[50]',
                'errors' => [
                    'required' => 'Họ tên không được để trống',
                    'min_length' => 'Họ tên có ít nhất 3 ký tự',
                    'max_length' => 'Họ tên có nhiều nhất 50 ký tự'
                ]
            ],
            'account_name' => [
                'rules' => 'required|min_length[3]|max_length[50]',
                'errors' => [
                    'required' => 'Tên đăng nhập không được để trống',
                    'min_length' => 'Tên đăng nhập có ít nhất 3 ký tự',
                    'max_length' => 'Tên đăng nhập có nhiều nhất 50 ký tự'
                ]
            ],
            'password_hash' => [
                'rules' => 'max_length[255]',
                'errors' => [
                    'max_length' => 'Mật khẩu quá dài'
                ]
            ],
            'password_salt' => [
                'rules' => 'max_length[255]',
                'errors' => [
                    'max_length' => 'Mật khẩu quá dài'
                ]
            ],
            'password' => [
                'rules' => 'min_length[6]|max_length[255]',
                'errors' => [
                    'min_length' => 'Mật khẩu có ít nhất 6 ký tự',
                    'max_length' => 'Mật khẩu quá dài'
                ]
            ],
        ];
    }

    public function get_bc_xe_chi_tiet($area_id, $select_date)
    {
        $array_result = [];
        $so_lieu = [];

        $bieu_do = [];
        $data_bieu_do = new \stdClass;
        $labels = [];
        $datasets = [];
        $dataset_slxv = new \stdClass();
        $dataset_slxv->label = 'SL xe vào';
        $dataset_slxv->data = [];
        $dataset_slxv->borderColor = 'rgb(95, 182, 99)';
        $dataset_slxv->fill = false;
        $dataset_slxv->tension = 0.1;
        $datasets[] = $dataset_slxv;

        $dataset_slxr = new \stdClass();
        $dataset_slxr->label = 'SL xe ra';
        $dataset_slxr->data = [];
        $dataset_slxr->borderColor = 'rgb(54, 162, 235)';
        $dataset_slxr->fill = false;
        $dataset_slxr->tension = 0.1;
        $datasets[] = $dataset_slxr;

        $dataset_slxkhl = new \stdClass();
        $dataset_slxkhl->label = 'SL xe không hợp lệ';
        $dataset_slxkhl->data = [];
        $dataset_slxkhl->borderColor = 'rgb(255, 159, 64)';
        $dataset_slxkhl->fill = false;
        $dataset_slxkhl->tension = 0.1;
        $datasets[] = $dataset_slxkhl;

//        [{
//            label: 'My First Dataset',
//                data: [65, 59, 80, 81, 56, 55, 40],
//                fill: false,
//                borderColor: 'rgb(75, 192, 192)',
//                tension: 0.1
//            }]

        $stt = 0;
        $sql_format_date_hour = '%Y-%m-%d %k'; //dinh dang cho sql format

        for ($hour = 0; $hour < 24; $hour++) {
            $labels[] = $hour; //set giá trị cột x cho chart


            //so luong xe vao
            $query_sl_xe_vao = 'select count(1) as count_sl_xe_vao from car_in_out_history where DATE_FORMAT(checkin_time,?) = ? and checkin_order >= 0 ';
            if ($area_id) {
                $query_sl_xe_vao .= ' and area_id = ' . $area_id;
            }
            $sql_data = $this->db->query($query_sl_xe_vao, [$sql_format_date_hour, $select_date . ' ' . $hour])->getRow();
            $slxv = 0;
            if ($sql_data) {
                $slxv = $sql_data->count_sl_xe_vao;
            }
            $datasets[0]->data[] = $slxv; //gán dữ liệu cho đường line xe vào trên chart

            //so luong xe ra
            $query_sl_xe_ra = 'select count(1) as count_sl_xe_ra from car_in_out_history where DATE_FORMAT(checkout_time,?) = ?   ';
            if ($area_id) {
                $query_sl_xe_ra .= ' and area_id = ' . $area_id;
            }
            $sql_data = $this->db->query($query_sl_xe_ra, [$sql_format_date_hour, $select_date . ' ' . $hour])->getRow();
            $slxr = 0;
            if ($sql_data) {
                $slxr = $sql_data->count_sl_xe_ra;
            }
            $datasets[1]->data[] = $slxr; //gán dữ liệu cho đường line xe ra trên chart


            //so luong xe khoong hop le
            $query_sl_xe_khl = 'select count(1) as count_sl_xe_khl from car_in_out_history where DATE_FORMAT(checkout_time,?) = ? and checkout_order_status = 0 ';
            if ($area_id) {
                $query_sl_xe_khl .= ' and area_id = ' . $area_id;
            }
            $sql_data = $this->db->query($query_sl_xe_khl, [$sql_format_date_hour, $select_date . ' ' . $hour])->getRow();
            $slxkhl = 0;
            if ($sql_data) {
                $slxkhl = $sql_data->count_sl_xe_khl;
            }
            $datasets[2]->data[] = $slxkhl; //gán dữ liệu cho đường line xe khl trên chart

            //co du lieu 3 cot thi dua vao
            if ($slxv || $slxr || $slxkhl) {
                $stt++;
                $item_sl = [];
                $item_sl['stt'] = $stt;
                $item_sl['gio'] = $hour;
                $item_sl['slxv'] = $slxv;
                $item_sl['slxr'] = $slxr;
                $item_sl['slxkhl'] = $slxkhl;
                $so_lieu[] = $item_sl;

            }
        }
        $data_bieu_do->labels = $labels;
        $data_bieu_do->datasets = $datasets;

        $array_result['so_lieu'] = $so_lieu;
        $array_result['bieu_do'] = json_encode($data_bieu_do);
        return $array_result;
    }

    public function get_bc_sp_chi_tiet($area_id, $select_date, $labels_query)
    {
        $array_result = [];
        $bieu_do = [];
        $data_bieu_do = new \stdClass;
        $labels = [];
        $datasets = [];
        $stt = 0;
        $sql_format_date_hour ='%Y-%m-%d %k';

        foreach ($labels_query as $key => $item){
            $dataset_sl = new \stdClass();
            $dataset_sl->label = $item['product_name'];
            $dataset_sl->data= [];
            $dataset_sl->fill= false;
            $dataset_sl->tension = 0.1;
            $color = array_rand($this->rgb, 1);
            $dataset_sl->borderColor = $this->rgb[$color];
            $datasets[] = $dataset_sl;
            unset($this->rgb[$color]);
            for($hour = 0; $hour < 24; $hour++){
                if (!in_array( $hour, $labels)){
                    $labels[] = $hour; //set giá trị cột x cho chart
                }
                $query_sl_data = 'select sum(tong_du_xuat) as sum_quantity from tgbx_order_detail 
                                                                      inner join pump_product_type
                                                                      on tgbx_order_detail.ma_hang_hoa = pump_product_type.product_type_code
                                                                      where DATE_FORMAT(check_time,?) = ? 
                                                                      AND ma_hang_hoa = ?';
                if($area_id){
                    $query_sl_data .= ' and area_id = ' . $area_id;
                }

                $sql_data = $this->db->query($query_sl_data,[$sql_format_date_hour,$select_date . ' '. $hour, $item['ma_hang_hoa']])->getResult('array');
                if ($sql_data['0']['sum_quantity']){
                    $item_sl[$item['ma_hang_hoa']] = $sql_data['0']['sum_quantity'];
                    $datasets[$stt]->data[] = $sql_data['0']['sum_quantity'];
                } else {
                    $datasets[$stt]->data[] = 0;
                }
            }
            $stt++;
        }

        $so_lieu = $this->get_data_table_sp($area_id, $select_date);

        $data_bieu_do->labels = $labels;
        $data_bieu_do->datasets = $datasets;
        $array_result['so_lieu'] = $so_lieu;
        $array_result['bieu_do'] = json_encode($data_bieu_do);
        return $array_result;
    }

    public function get_data_table_sp($area_id, $select_date){
        $stt = 0;
        $so_lieu = [];
        $sql_format_date_hour ='%Y-%m-%d %k';
        for($hour = 0; $hour < 24; $hour++)
        {
            $query_sl_data = 'select ma_hang_hoa, sum(tong_du_xuat) as sum_quantity from tgbx_order_detail 
                                                                      inner join pump_product_type
                                                                      on tgbx_order_detail.ma_hang_hoa = pump_product_type.product_type_code
                                                                      where DATE_FORMAT(check_time,?) = ?';
            if($area_id){
                $query_sl_data .= ' and area_id = ' . $area_id;
            }

            $query_sl_data .= ' group by ma_hang_hoa';

            $sql_data = $this->db->query($query_sl_data,[$sql_format_date_hour,$select_date . ' '. $hour])->getResult('array');
            if ($sql_data){
                $item_sl = [];
                $stt++;
                $item_sl['gio'] = $hour;
                foreach ($sql_data as $key => $item){
                    $item_sl[$item['ma_hang_hoa']] = $item['sum_quantity'];
                }
                $item_sl['stt'] = $stt;
                $so_lieu[] = $item_sl;
            }
        }
        return $so_lieu;
    }

    public function get_bc_xe_tong_hop($area_id, $start_date, $end_date)
    {

        $array_result = [];
        $so_lieu = [];

        $bieu_do = [];
        $data_bieu_do = new \stdClass;
        $labels = [];
        $datasets = [];
        $dataset_slxv = new \stdClass();
        $dataset_slxv->label = 'SL xe vào';
        $dataset_slxv->data = [];
        $dataset_slxv->borderColor = 'rgb(95, 182, 99)';
        $dataset_slxv->fill = false;
        $dataset_slxv->tension = 0.1;
        $datasets[] = $dataset_slxv;

        $dataset_slxr = new \stdClass();
        $dataset_slxr->label = 'SL xe ra';
        $dataset_slxr->data = [];
        $dataset_slxr->borderColor = 'rgb(54, 162, 235)';
        $dataset_slxr->fill = false;
        $dataset_slxr->tension = 0.1;
        $datasets[] = $dataset_slxr;

        $dataset_slxkhl = new \stdClass();
        $dataset_slxkhl->label = 'SL xe không hợp lệ';
        $dataset_slxkhl->data = [];
        $dataset_slxkhl->borderColor = 'rgb(255, 159, 64)';
        $dataset_slxkhl->fill = false;
        $dataset_slxkhl->tension = 0.1;
        $datasets[] = $dataset_slxkhl;

//        [{
//            label: 'My First Dataset',
//                data: [65, 59, 80, 81, 56, 55, 40],
//                fill: false,
//                borderColor: 'rgb(75, 192, 192)',
//                tension: 0.1
//            }]

        $stt = 0;
        $sql_format_date = '%Y-%m-%d'; //dinh dang cho sql format
        $period = new DatePeriod(new DateTime($start_date), new DateInterval('P1D'), new DateTime($end_date . '+' . '1 day'));
        foreach ($period as  $date) {
            $dates =  $date->format("Y-m-d");
            $labels[] = $dates ;
            //so luong xe vao
            $query_sl_xe_vao = 'select count(1) as count_sl_xe_vao from car_in_out_history where DATE_FORMAT(checkin_time,?) = ? and checkin_order >= 0 ';
            if ($area_id) {
                $query_sl_xe_vao .= ' and area_id = ' . $area_id;
            }

            $sql_data = $this->db->query($query_sl_xe_vao, [$sql_format_date,$dates])->getRow();
            $slxv = 0;
            if ($sql_data) {
                $slxv = $sql_data->count_sl_xe_vao;
            }
            $datasets[0]->data[] = $slxv; //gán dữ liệu cho đường line xe vào trên chart

            //so luong xe ra
            $query_sl_xe_ra = 'select count(1) as count_sl_xe_ra from car_in_out_history where DATE_FORMAT(checkout_time,?) = ? ';
            if ($area_id) {
                $query_sl_xe_ra .= ' and area_id = ' . $area_id;
            }
            $sql_data = $this->db->query($query_sl_xe_ra, [$sql_format_date,$dates])->getRow();
            $slxr = 0;
            if ($sql_data) {
                $slxr = $sql_data->count_sl_xe_ra;
            }
            $datasets[1]->data[] = $slxr; //gán dữ liệu cho đường line xe ra trên chart


            //so luong xe khoong hop le
            $query_sl_xe_khl = 'select count(1) as count_sl_xe_khl from car_in_out_history where DATE_FORMAT(checkout_time,?) = ? and checkout_order_status = 0 ';
            if ($area_id) {
                $query_sl_xe_khl .= ' and area_id = ' . $area_id;
            }
            $sql_data = $this->db->query($query_sl_xe_khl, [$sql_format_date,$dates])->getRow();
            $slxkhl = 0;
            if ($sql_data) {
                $slxkhl = $sql_data->count_sl_xe_khl;
            }
            $datasets[2]->data[] = $slxkhl; //gán dữ liệu cho đường line xe khl trên chart

            //co du lieu 3 cot thi dua vao
            if ($slxv || $slxr || $slxkhl) {
                $stt++;
                $item_sl = [];
                $item_sl['stt'] = $stt;
                $item_sl['ngay'] = $dates;
                $item_sl['slxv'] = $slxv;
                $item_sl['slxr'] = $slxr;
                $item_sl['slxkhl'] = $slxkhl;
                $so_lieu[] = $item_sl;

            }
        }

        $data_bieu_do->labels = $labels ;
        $data_bieu_do->datasets = $datasets;

        $array_result['so_lieu'] = $so_lieu;
        $array_result['bieu_do'] = json_encode($data_bieu_do);
        return $array_result;
    }


    public function get_bc_sp_tong_hop($area_id, $from_date, $to_date, $labels_table)
    {
        $bieu_do = [];
        $data_bieu_do = new \stdClass;
        $labels  = [];
        $datasets =[];
        $stt = 0;
        $sql_format_date_hour ='%Y-%m-%d';
        $period = new \DatePeriod(new \DateTime($from_date), new \DateInterval('P1D'), new \DateTime($to_date . '+' . '1 day'));
        foreach($labels_table as $key => $item) {
            $dataset_sl = new \stdClass();
            $dataset_sl->label = $item['product_name'];
            $dataset_sl->data= [];
            $dataset_sl->fill= false;
            $dataset_sl->tension = 0.1;
            $color = array_rand($this->rgb, 1);
            $dataset_sl->borderColor = $this->rgb[$color];
            unset($this->rgb[$color]);
            $datasets[] = $dataset_sl;
            $data_total[$item['ma_hang_hoa']] = 0;
            foreach ($period as $date){
                $dates =  $date->format("Y-m-d");
                if (!in_array( $dates, $labels)){
//                    $labels[0] = '0';
                    $labels[] = $dates; //set giá trị cột x cho chart
                }
                $query_sl_data = 'select ma_hang_hoa, sum(tong_du_xuat) as sum_quantity from tgbx_order_detail
                                                                      inner join pump_product_type
                                                                      on tgbx_order_detail.ma_hang_hoa = pump_product_type.product_type_code
                                                                      where DATE_FORMAT(check_time,?) = ?
                                                                      AND ma_hang_hoa = ?';
                if($area_id){
                    $query_sl_data .= ' and area_id = ' . $area_id;
                }

                $sql_data = $this->db->query($query_sl_data,[$sql_format_date_hour,$dates, $item['ma_hang_hoa']])->getRow();
                $datasets[$stt]->data[0] = 0;
                if ($sql_data->sum_quantity){
                    $item_sl[$item['ma_hang_hoa']] = $sql_data->sum_quantity;
                    $datasets[$stt]->data[] = $sql_data->sum_quantity;
                    $data_total[$item['ma_hang_hoa']] += $sql_data->sum_quantity;
                }else{
                    $datasets[$stt]->data[] = 0;
                }
            }
            $stt++;
        }

        $so_lieu = $this->get_data_table_tong_hop($area_id, $from_date, $to_date);

        $data_bieu_do->labels = $labels;
        $data_bieu_do->datasets = $datasets;
        $array_result['so_lieu'] = $so_lieu;
        $array_result['data_total'] =  $data_total;
        $array_result['bieu_do'] = json_encode($data_bieu_do);
        return $array_result;
    }

    public function get_data_table_tong_hop($area_id, $from_date, $to_date)
    {
        $stt = 0;
        $so_lieu = [];
        $sql_format_date_hour ='%Y-%m-%d';
        $period = new \DatePeriod(new \DateTime($from_date), new \DateInterval('P1D'), new \DateTime($to_date . '+' . '1 day'));
        foreach ($period as $key => $item)
        {
            $dates = $item->format("Y-m-d");
            $query_sl_data = 'select ma_hang_hoa, sum(tong_du_xuat) as sum_quantity, pump_product_type.product_name from tgbx_order_detail
                                                                      inner join pump_product_type
                                                                      on tgbx_order_detail.ma_hang_hoa = pump_product_type.product_type_code
                                                                      where DATE_FORMAT(check_time,?) = ?';
            if($area_id){
                $query_sl_data .= ' and area_id = ' . $area_id;
            }

            $query_sl_data .= ' group by ma_hang_hoa';

            $sql_data = $this->db->query($query_sl_data,[$sql_format_date_hour,$dates])->getResult('array');
            if ($sql_data){
                $item_sl = [];
                $stt++;
                $item_sl['date'] = $dates;
                foreach ($sql_data as $key => $item){
                    $item_sl[$item['ma_hang_hoa']] = $item['sum_quantity'];
                }
                $item_sl['stt'] = $stt;
                $so_lieu[] = $item_sl;
            }
        }
        return $so_lieu;
    }

    public function get_bc_sp_hong_tong_hop($area_id, $from_date, $to_date, $labels_table)
    {
        $bieu_do = [];
        $data_bieu_do = new \stdClass;
        $labels  = [];
        $datasets =[];
        $stt = 0;
        $sql_format_date_hour ='%Y-%m-%d';
        $period = new \DatePeriod(new \DateTime($from_date), new \DateInterval('P1D'), new \DateTime($to_date . '+' . '1 day'));
        foreach($labels_table as $key => $item) {
            $dataset_sl = new \stdClass();
            $dataset_sl->label = $item['throad_name'];
            $dataset_sl->data= [];
            $dataset_sl->fill= false;
            $dataset_sl->tension = 0.1;
            $color = array_rand($this->rgb, 1);
            $dataset_sl->borderColor = $this->rgb[$color];
            unset($this->rgb[$color]);
            $datasets[] = $dataset_sl;
            $data_total[$item['throad_id']] = 0;
            foreach ($period as $date){
                $dates =  $date->format("Y-m-d");
                if (!in_array( $dates, $labels)){
//                    $labels[0] = '0';
                    $labels[] = $dates; //set giá trị cột x cho chart
                }
                $query_sl_data = 'select throad_name, sum(tong_du_xuat) as sum_quantity from tgbx_order_detail
                                                                        where DATE_FORMAT(check_time,?) = ?
                                                                        AND throad_id = ?';
                if($area_id){
                    $query_sl_data .= ' and area_id = ' . $area_id;
                }

                $sql_data = $this->db->query($query_sl_data,[$sql_format_date_hour,$dates, $item['throad_id']])->getRow();
//                $datasets[$stt]->data[0] = 0;
                if ($sql_data->sum_quantity){
                    $item_sl[$item['throad_id']] = $sql_data->sum_quantity;
                    $datasets[$stt]->data[] = $sql_data->sum_quantity;
                    $data_total[$item['throad_id']] += $sql_data->sum_quantity;
                }else{
                    $datasets[$stt]->data[] = 0;
                }
            }
            $stt++;
        }

        $so_lieu = $this->get_data_table_hong_tong_hop($area_id, $from_date, $to_date);

        $data_bieu_do->labels = $labels;
        $data_bieu_do->datasets = $datasets;
        $array_result['so_lieu'] = $so_lieu;
        $array_result['data_total'] =  $data_total;
        $array_result['bieu_do'] = json_encode($data_bieu_do);
        return $array_result;
    }

    public function get_data_table_hong_tong_hop($area_id, $from_date, $to_date)
    {
        $stt = 0;
        $so_lieu = [];
        $sql_format_date_hour ='%Y-%m-%d';
        $period = new \DatePeriod(new \DateTime($from_date), new \DateInterval('P1D'), new \DateTime($to_date . '+' . '1 day'));
        foreach ($period as $key => $item)
        {
            $dates = $item->format("Y-m-d");
            $query_sl_data = 'select throad_id, sum(tong_du_xuat) as sum_quantity, tgbx_order_detail.throad_name 
                                                                      from tgbx_order_detail
                                                                      where DATE_FORMAT(check_time,?) = ?';
            if($area_id){
                $query_sl_data .= ' and area_id = ' . $area_id;
            }

            $query_sl_data .= ' group by throad_id';

            $sql_data = $this->db->query($query_sl_data,[$sql_format_date_hour,$dates])->getResult('array');
            if ($sql_data){
                $item_sl = [];
                $stt++;
                $item_sl['date'] = $dates;
                foreach ($sql_data as $key => $item){
                    $item_sl[$item['throad_id']] = $item['sum_quantity'];
                }
                $item_sl['stt'] = $stt;
                $so_lieu[] = $item_sl;
            }
        }
        return $so_lieu;
    }

    public function get_bc_hong_chi_tiet($area_id, $select_date, $labels_query)
    {
        $array_result = [];
        $bieu_do = [];
        $data_bieu_do = new \stdClass;
        $labels = [];
        $datasets = [];
        $stt = 0;
        $sql_format_date_hour ='%Y-%m-%d %k';

        foreach ($labels_query as $key => $item){
            $dataset_sl = new \stdClass();
            $dataset_sl->label = $item['throad_name'];
            $dataset_sl->data= [];
            $dataset_sl->fill= false;
            $dataset_sl->tension = 0.1;
            $color = array_rand($this->rgb, 1);
            $dataset_sl->borderColor = $this->rgb[$color];
            $datasets[] = $dataset_sl;
            unset($this->rgb[$color]);
            $data_total[$item['throad_id']] = 0;
            for($hour = 0; $hour < 24; $hour++){
                if (!in_array( $hour, $labels)){
                    $labels[] = $hour; //set giá trị cột x cho chart
                }
                $query_sl_data = 'select throad_name, sum(tong_du_xuat) as sum_quantity from tgbx_order_detail
                                                                        where DATE_FORMAT(check_time,?) = ?
                                                                        AND throad_id = ?';
                if($area_id){
                    $query_sl_data .= ' and area_id = ' . $area_id;
                }

                $sql_data = $this->db->query($query_sl_data,[$sql_format_date_hour,$select_date . ' '. $hour, $item['throad_id']])->getRow();
//                $datasets[$stt]->data[0] = 0;
                if ($sql_data->sum_quantity){
                    $item_sl[$item['throad_id']] = $sql_data->sum_quantity;
                    $datasets[$stt]->data[] = $sql_data->sum_quantity;

                    $data_total[$item['throad_id']] += $sql_data->sum_quantity;
                }else{
                    $datasets[$stt]->data[] = 0;
                }
            }
            $stt++;
        }

        $so_lieu = $this->get_data_table_hong_chi_tiet($area_id, $select_date);

        $data_bieu_do->labels = $labels;
        $data_bieu_do->datasets = $datasets;
        $array_result['so_lieu'] = $so_lieu;
        $array_result['data_total'] =  $data_total;
        $array_result['bieu_do'] = json_encode($data_bieu_do);
        return $array_result;
    }

    public function get_data_table_hong_chi_tiet($area_id, $select_date){
        $stt = 0;
        $so_lieu = [];
        $sql_format_date_hour ='%Y-%m-%d %k';
        for($hour = 0; $hour < 24; $hour++)
        {
            $query_sl_data = 'select throad_id, sum(tong_du_xuat) as sum_quantity, tgbx_order_detail.throad_name 
                                                                      from tgbx_order_detail
                                                                      where DATE_FORMAT(check_time,?) = ?';
            if($area_id){
                $query_sl_data .= ' and area_id = ' . $area_id;
            }

            $query_sl_data .= ' group by throad_id';

            $sql_data = $this->db->query($query_sl_data,[$sql_format_date_hour,$select_date . ' '. $hour])->getResult('array');
            if ($sql_data){
                $item_sl = [];
                $stt++;
                $item_sl['gio'] = $hour;
                foreach ($sql_data as $key => $item){
                    $item_sl[$item['throad_id']] = $item['sum_quantity'];
                }
                $item_sl['stt'] = $stt;
                $so_lieu[] = $item_sl;
            }
        }
        return $so_lieu;
    }
}
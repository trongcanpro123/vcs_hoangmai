<?php

namespace App\Models;


use App\Helpers\StringHelper;
use App\Models\Interfaces\ContentInterface;
use App\Models\Interfaces\ImageAssetInterface;

/**
 * Class ContentCategoryModel
 * @package App\Models
 *
 * @property int $car_number
 * @property string $note
 */
class DeliveryModel extends BaseModel
{
    protected $table = 'delivery_config';
    protected $primaryKey = 'id';

    protected $useSoftDeletes = false;
    protected $allowedFields = ['delivery_name'];
    public function select_delivery_id($id)
    {
        return $this->db->query('SELECT * FROM delivery_config WHERE id = ?', [$id])->getRow();
    }
    public function select_delivery_id_arr($id)
    {
        return $this->db->query('SELECT * FROM delivery_config WHERE id = ?', [$id])->getResultArray();
    }
     /**
     * @param string|null $scenario
     * @return array
     */
    public function getRules(string $scenario = null): array
    {
        return [
    
        ];
    }
}
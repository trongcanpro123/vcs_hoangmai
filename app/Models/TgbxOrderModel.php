<?php


namespace App\Models;
use App\Helpers\StringHelper;
use App\Models\Interfaces\ContentInterface;

class TgbxOrderModel extends BaseModel
{
    protected $table = 'tgbx_order';
    protected $primaryKey = 'id';

    protected $useSoftDeletes = false;
    protected $allowedFields = ['ma_lenh', 'so_lenh', 'ngay_xuat', 'ma_phuong_tien', 'nguoi_van_chuyen', 'status', 'area_id', 'in_out_id'];

    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $dateFormat = 'int';

    /**
     * @param string|null $scenario
     * @return array
     */
    public function getRules(string $scenario = null): array
    {
        return [
        ];
    }
}
<?php

use App\Helpers\Html;

/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\AdministratorModel[] $models
 * @var \CodeIgniter\Pager\Pager $pager
 */

$this->title = 'In ticket';
?>

<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-text card-header-info">
                        <div class="card-icon">
                            <i class="material-icons">search</i>
                        </div>

                    </div>
                    <div class="card-body ">
                        <form  method="get">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class='input-group date'>
                                        <input type='text' name="car_number" placeholder="Biển số" style="text-transform:uppercase"
                                               class="form-control " autocomplete="off"
                                               value="<?= $param_search?$param_search['car_number']:'' ?>">
                                    </div>
                                </div>

                                <div class="col-md-2 text-center">
                                    <button class="btn btn-info btn-round" type="submit">Tìm kiếm</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header card-header-info flex-align card-header-text"  >
                        <div class="card-text" >
                            <h4 class="card-title">Thông tin</h4>
                        </div>
                    </div>
                    <div class="card-body table-responsive">
                        <?php if($model) {
                            ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>Thời gian: <?= $model->checkin_time? date('d-m-Y H:i:s', strtotime($model->checkin_time)): '' ?></h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>Biển số xe: <?= $model->car_number ?></h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <h4>Trạng thái: <?= $model->ticket?'Đã phát hành Ticket':'Chưa phát hành Ticket' ?></h4>
                                </div>
                            </div>
                        <?php
                        } ?>


                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>Số lệnh</th>
                                <th>Mã hàng</th>
                                <th>Tên hàng</th>
                                <th>Số lượng (L)</th>
                                <th>Giàn</th>
                                <th>Họng</th>
                            </tr>
                            </thead>
                            <tbody id="myTable">

                            <?php if ($list_tgbx_detail): ?>
                                <?php foreach ($list_tgbx_detail as $item): ?>
                                    <tr>
                                        <td><?= $item->so_lenh ?></td>
                                        <td><?= $item->ma_hang_hoa ?></td>
                                        <td><?= $item->get_ten_hang() ?></td>
                                        <td><?= number_format($item->tong_du_xuat) ?></td>
                                        <td><?= $item->pump_name ?></td>
                                        <td><?= $item->throad_name ?></td>


                                    </tr>
                                <?php endforeach; ?>
                            <?php endif ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

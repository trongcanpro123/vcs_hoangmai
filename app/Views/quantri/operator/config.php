<?php

use App\Helpers\Html;

/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\AdministratorModel[] $models
 * @var \CodeIgniter\Pager\Pager $pager
 */

$this->title = 'Cấu hình';
?>
<div class="card">
    <div class="card-header card-header-info flex-align">
        <div>
            <h4 class="card-title"><?= $this->title ?></h4>
        </div>
        <a href="<?= route_to('nv_config_create') ?>" class="btn btn-warning btn-round btn-sm">Thêm mới</a>
    </div>
    <div class="card-body table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Họng bơm</th>
                <th>Giàn</th>
                <th>Mã hàng</th>
                <th>Tên hàng</th>
                <th>Công suất(L/phút)</th>
                <th>Trạng thái</th>
                <th>Màn hình</th>
                <th>Hành động</th>
            </tr>
            </thead>
            <tbody>
            <?php if (!$models || empty($models)): ?>
                <tr>
                    <td colspan="100">
                        <div class="empty-block">
                            <img src="/images/no-content.jpg" alt="No content"/>
                            <h4>Không có nội dung</h4>
                            <a class="btn btn-info btn-round"
                               href="<?= route_to('administrator_create') ?>">Thêm</a>
                        </div>
                    </td>
                </tr>
            <?php else: ?>
                <?php foreach ($models as $model): ?>
                    <tr>
                        <td><?= $model->throad_name ?></td>
                        <td><?= $model->select_pump_system() ?></td>
                        <?php $pump_prd_type =  $model->select_pump_product_type($model->product_type_id);  ?>
                        <td><?=$pump_prd_type->product_type_code?></td>
                        <td><?=$pump_prd_type->product_name?></td>
                        <td><?= number_format($model->wattage) ?></td>
                        <td><?php if ($model->active == 1) {
                                echo "Hoạt động";
                            } else {
                                echo "Tạm dừng";
                            } ?></td>
                        <td>Màn <?= $model->display_on ?></td>
                        <td class="row-actions">
                            <a href="<?= route_to('nv_config_update', $model->getPrimaryKey()) ?>"
                               class="btn btn-info btn-just-icon btn-sm" title="sửa">
                                <i class="material-icons">edit</i>
                            </a>
                            <a href="<?= route_to('nv_config_delete', $model->getPrimaryKey()) ?>"
                               class="btn btn-danger btn-just-icon btn-sm" data-method="post" title="xóa"
                               data-prompt="Bạn có chắc muốn xoá cấu hình này?">
                                <i class="material-icons">delete</i>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif ?>
            </tbody>
        </table>
        <?= $pager->links('default', 'default_cms') ?>
    </div>
</div>

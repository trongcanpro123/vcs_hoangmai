<?php

use App\Helpers\Html;

/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\ProjectCategoryModel $model
 * @var \CodeIgniter\Validation\Validation $validator
 */
$this->title = 'Thông Báo Cho Xe Ra';
?>
<!doctype html>
<html>
<head>

    <title><?= $this->title ?></title>
    <link rel="shortcut icon" type="image/png" href="/favicon.ico"/>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no'
          name='viewport'/>

</head>
<body>
<div style="text-align: center; margin-top: 300px; color: red">
    <?php if($alert){
        ?>
        <h1 style="text-transform: uppercase;"><?= $alert->message ?></h1>

        <?php
    }else{
        ?>
        <h1 style="text-transform: uppercase;">Màn hình thông báo xe ra</h1>

        <?php
    } ?>
</div>
</body>
</html>
<script>
    window.onload = function() {

        var t = setInterval(reload_this_page,1000);

    };
    function reload_this_page(){
        location.reload();
    }
</script>
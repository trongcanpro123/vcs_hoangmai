<?php

use App\Helpers\Html;

/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\ProjectCategoryModel $model
 * @var \CodeIgniter\Validation\Validation $validator
 */
$this->title = 'Tạo mới xe';
?>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-info flex-align">
                <div>
                    <h4 class="card-title"><?= $this->title ?></h4>
                </div>
            </div>
            <div class="card-body">
                <form action="<?='/quantri/car/create' . ($in_out_id?('/'.$in_out_id):'') ?>" method="post" enctype="multipart/form-data">
                    <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="bmd-label-floating">Biển số xe</label>
                            <?= Html::textInput('car_number', $model->car_number, [
                                'autocomplete' => 'off',
                                'class' => 'form-control',
                                'disabled'=> $readonly ? true : false,
                                'autofocus' => true,
                            ]) ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="bmd-label-floating">Tên tài xế</label>
                            <?= Html::textInput('drive_name', $model->drive_name, [
                                'autocomplete' => 'off',
                                'class' => 'form-control',
                                'disabled'=> $readonly ? true : false,
                                'autofocus' => true,
                            ]) ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="bmd-label-floating">Loại xe</label>
                            <?= Html::textInput('car_type', $model->car_type, [
                                'autocomplete' => 'off',
                                'class' => 'form-control',
                                'disabled'=> $readonly ? true : false,
                                'autofocus' => true,
                            ]) ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="bmd-label-floating">Đơn vị</label>
                            <?= Html::textInput('delivery_unit', $model->delivery_unit, [
                                'autocomplete' => 'off',
                                'class' => 'form-control',
                                'disabled'=> $readonly ? true : false,
                                'autofocus' => true,
                            ]) ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="bmd-label-floating">Lưu ý</label>
                            <?= Html::textInput('note', $model->note, [
                                'autocomplete' => 'off',
                                'class' => 'form-control',
                                'disabled'=> $readonly ? true : false,
                                'autofocus' => true,
                            ]) ?>
                        </div>
                    </div>
                    <div class="row">
                                    <div class="col-md-12 text-center">
                                            <a href="<?= route_to('bv_list_cars') ?>" class="btn btn-round">Huỷ</a>
                                            <button style="margin-left:15px" class="btn btn-success btn-round"
                                                    type="submit">Lưu
                                            </button>
                                        </div>
                                    </div>
                    
                </div>
                        <?php if ($validator): ?>
                            <div class="alert alert-danger" style="margin-top: 32px;">
                                <ul style="margin: 0; padding-left: 16px;">
                                    <?php foreach ($validator->getErrors() as $error): ?>
                                        <li><?= Html::decode($error) ?></li>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                        <?php endif; ?>
                    </div>
            </div>
        </div>
    </div>

    <script>
        window.onload = function () {
            setTimeout(function(){
                if($('#videoElement').attr('src')){
                    snap_video('videoElement','img_snap_front','image_front');
                    snap_video('videoElement','img_snap_right','image_car');
                    var _data = {
                        image_front: $('#image_front').val(),
                    }
                    $.post('/Admin/GateKeeper/bv_detect_car_number', _data, function (res) {
                        var result = JSON.parse(res);
                        if(result){
                            if(!$('input[name="car_number"]').val()){
                                $('input[name="car_number"]').val(result);
                                $('input[name="car_number"]').focus();
                            }
                        }
                    });
                }

            }, 2000);

        };
    </script>
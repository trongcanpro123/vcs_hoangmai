<?php

use App\Helpers\Html;

/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\ProjectModel[] $models
 * @var \CodeIgniter\Pager\Pager $pager
 */

$this->title = 'Lịch sử vào ra';
?>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header card-header-text card-header-info">
                <div class="card-icon">
                    <i class="material-icons">search</i>
                </div>
            </div>
            <div class="card-body ">
                <form action="<?= route_to('bv_in_out_history') ?>" method="GET">
                    <div style="margin-bottom: 10px" class="row">
                        <div class="col-md-6">
                            <input placeholder="Biển số xe" type="text" name="car_number" autocomplete="off" class="form-control" autofocus=""
                                   value="<?= $param_search['car_number'] ?>">
                        </div>
                        <div class="col-md-2">
                        <input type="submit" autocomplete="off" class="btn btn-info btn-round " value="Tìm kiếm">
                        </div>
                    </div>
                    
            </div>
            </form>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header card-header-info flex-align">
        <div>
            <h4 class="card-title"><?= $this->title ?></h4>
        </div>
    </div>
    <div class="card-body table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>STT</th>
                <th>Biển số</th>
                <th>Loại xe</th>
                <th>Đơn vị giao hàng</th>
                <th>Loại vật liệu</th>
                <th>Phương pháp giao nhận</th>
                <th>Khối lượng nhập</th>
                <th>Khối lượng giảm trừ</th>
                <th>Thực nhập</th>
                <th>Thời gian vào</th>
                <th>Khối gian ra</th>
                <th>Trạng thái duyệt yêu cầu</th>
                <th>Hành động</th>
            </tr>
            </thead>
            <tbody>
            <?php if (!$models || empty($models)): ?>
                <tr>
                    <td colspan="100">
                        <div class="empty-block">
                            <img src="/images/no-content.jpg" alt="No content"/>
                            <h4>Không có nội dung</h4>
                            <a class="btn btn-info btn-round"
                               href="<?= route_to('admin_driver_create') ?>">Thêm</a>
                        </div>
                    </td>
                </tr>
            <?php else: ?>
                <?php foreach ($models as $key => $model): ?>
                    <tr>
                        <td class="row-actions text-center"><?= ++$key ?></td>
                        <td><?= Html::decode($model->driver_name) ?></td>
                        <td>
                            <?= $model->car_number ?>
                        </td>
                        <td>
                            <?= $model->car_number ?>
                        </td>
                        <td>
                            <?= $model->car_number ?>
                        </td>
                        <td>
                            <?= $model->car_number ?>
                        </td>
                        <td>
                            <?= $model->car_number ?>
                        </td>
                        <td>
                            <?= $model->car_number ?>
                        </td>
                        <td>
                            <?= $model->car_number ?>
                        </td>

                        <td>
                            <?= $model->car_number ?>
                        </td>
                        <td>
                            <?= $model->car_number ?>
                        </td>
                        <td>
                            <?= $model->car_number ?>
                        </td>
                        <td class="row-actions">
                            <?php if($model->checkin_order == 0): ?>
                                <a href="/quantri/xu-ly-check-in/<?= $model->id ?>" type="button" class="btn btn-info btn-round btn-sm" >
                                   Tiếp tục
                                </a>
                            <?php endif; ?>
                            <?php if($model->checkin_order > 0): ?>
                                <a href="#" onclick="re_print_order(<?= $model->id ?>);" title="In STT"  type="button" class="btn btn-success btn-round btn-sm">
                                    <i class="material-icons">print</i>
                                </a>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif ?>
            </tbody>
        </table>
        <?= $pager->links('default', 'default_cms') ?>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="comfirm-checkin" tabindex="-1" role="dialog" aria-labelledby="comfirm-checkin" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="comfirm-checkin">Cấp số cho tài xế</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-footer">
                <button type="button" id="btn-assign-number" class="btn btn-primary ">Cấp số</button>
                <button type="button" id="btn-print-number" class="btn btn-primary">In số</button>
            </div>
        </div>
    </div>
</div>

<style>
.image-driver{
    height:350px;
    width: 300px
}
.mr-bt {
    margin-bottom:25px;
    text-align:center;
}
.image-title {

}
.div-image {
    margin-bottom:15px;
}
.btn-capture {
    min-width:100px;
    padding: 7px 24px;
}
</style>

<?php

use App\Helpers\Html;

/**
 * @var \App\Models\DriverModel $model
 */
?>

<div class="row">
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-12">
                <div class="card" style="margin-bottom: 0px">
                    <!-- <div class="card-header card-header-text card-header-info">
                        <div class="card-text">
                            <h6 type="hidden" class="card-title">Chọn camera</h6>
                        </div>
                    </div> -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="">
                                <select type="hidden" class="form-control " placeholder="Chọn Camera Ra" data-style="btn btn-link" id="select_camera_out" >
                                        <?php if($camera_in) {
                                            foreach ($camera_in as $camera){
                                                ?>
                                                <option data-camera_out_id ="<?= $camera->id ?>" value="<?= $camera->get_link_play() ?>" ><?= $camera->title ?></option>

                                                <?php
                                            }
                                        } ?>

                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <video width="100%" id="videoElement"></video>
                        <canvas style="display: none" id="canvas_videoElement" ></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="row">
            <div class="col-md-12">
                <div class="card" style="margin-bottom: 0px">
                    <!-- <div class="card-header card-header-text card-header-info">
                        <div class="card-text">
                            <h6 class="card-title">Chọn camera</h6>
                        </div>
                    </div> -->
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="">
                                    <select class="form-control " data-style="btn btn-link" id="select_camera_in" onchange="change_play_cam(this, 'select_camera_out')">
                                        <?php if($camera_out) {
                                            foreach ($camera_out as $camera){
                                                ?>
                                                <option data-camera_out_id ="<?= $camera->id ?>" value="<?= $camera->get_link_play() ?>" ><?= $camera->title ?></option>

                                                <?php
                                            }
                                        } ?>

                                    </select>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <video width="100%" id="videoElement_in"></video>
                        <canvas style="display: none" id="canvas_videoElement" ></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

<!--    right-->
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                       
                        <label class="bmd-label-floating">Công trình</label>
                            <select class="oneclick_create form-control" name="construction_name" id="construction_name">
                            <?php if ($model_config): ?>
                                        <?php foreach ($model_config as $model): ?>
                                            <option value="<?= $model->construction_name ?>"><?= $model->construction_name?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                            </select>
                            <input type="hidden" name="construction_id" value="<?= $model->id?>"/>
                            <input type="hidden" name="construction_name" value="<?= $model->construction_name?>"/>
                            <input type="hidden" name="construction_address" value="<?= $model->construction_address?>"/>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="bmd-label-floating">Phiếu nhập kho</label>
                            <?= Html::textInput('receipt', $model->receipt, [
                                'autocomplete' => 'off',
                                'class' => 'form-control',
                                'readOnly'=> true,
                                'autofocus' => true,
                            ]) ?>
                        </div>
                        
                    </div>
                
                
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="bmd-label-floating">Địa điểm</label>
                            <?= Html::textInput('address', $model->construction_address, [
                                'autocomplete' => 'off',
                                'class' => 'form-control',
                                'readOnly'=> true,
                                'autofocus' => true,
                            ]) ?>
                        </div>
                    </div>
                    </div>
                    <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="bmd-label-floating">Biển số xe</label>
                            <select class="oneclick_create form-control" name="car_id" id="id" onchange="change_car_number()">
                            <?php if ($model_car): ?>
                                        <?php foreach ($model_car as $model): ?>
                                            <option value="<?= $model->id?>"><?= $model->car_number?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                            </select>
                            <!-- <input type="hidden" name="car_id" value="<?= $model->id?>"/>
                            <input type="hidden" name="delivery_unit" value="<?= $model->delivery_unit?>"/>
                            <input type="hidden" name="car_type" value="<?= $model->car_type?>"/>
                            <input type="hidden" name="car_number" value="<?= $model->car_number?>"/> -->
                            <!-- <input type="hidden" name="receipt" value="<?= $model->receipt?>"/> -->
                        </div>
                    </div>
                </div>
                <div class="row">
                <div class="col-md-6">
                <div class="form-group bmd-form-group">
                 <label class="bmd-label-floating">Loại xe</label>
                            <input id="form_car_type" type="text" readOnly="true" value="<?= $model->car_type ?>"
                                   style="text-transform:uppercase" class="form-control">
                        </div>
                 </div>
                 <div class="col-md-6">
                 <div class="form-group bmd-form-group">
                 <label class="bmd-label-floating">Đơn vị giao hàng</label>
                            <input id="form_delivery_unit" type="text" readOnly="true" value="<?= $model->delivery_unit ?>"
                                   style="text-transform:uppercase" class="form-control">
                        </div>
                 </div>
                </div>
                <!-- <div class="row">
                    <div id="test" class="col-sm-6 <?= $model->delivery_unit?> ">
                    <label class="bmd-label-floating">Đơn vị giao hàng</label>
                        <div class="form-group bmd-form-group">
                            <input id="form_delivery_unit" type="text" autocomplete="off" placeholder="Đơn vị giao hàng" value="<?= $model->delivery_unit?>">
                        </div>
                    </div>
                    <div id="test" class="col-sm-6 <?= $model->car_type?> ">
                    <label class="bmd-label-floating">Loại xe</label>
                        <div class="form-group bmd-form-group">
                            <input id="form_car_type" type="text" autocomplete="off" placeholder="Loại xe" value="<?= $model->car_type ?>">
                        </div>
                    </div>
                </div> -->
                <div class="row">
                    <div id="test" class="col-sm-6 <?= $model->material_name?> ">
                    <label class="bmd-label-floating">Loại vật liệu</label>
                    <select class="oneclick_create form-control" name="material_id" id="material_name" onchange="change_material()">
                    <?php if ($model_material): ?>
                                        <?php foreach ($model_material as $model): ?>
                                            <option value="<?= $model->id ?>"><?= $model->material_name?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                            </select>
                    </div>
                    <label class="bmd-label-floating">Mã vật liệu</label>
                        <div class="form-group bmd-form-group">
                            <input id="form_material_code" type="text" readOnly="true" value="<?= $model->material_code ?>"
                                   style="text-transform:uppercase" class="form-control">
                        </div>
                    </div>
                </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                                <label class="bmd-label-floating">Khối lượng nhập</label>
                                <input id="form_input_volume" type="number" value="<?=$model->input_volume?>" name="input_volume"  onchange="change_input_material()" autocomplete="off" class="form-control">
                            </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="bmd-label-floating">Khối lượng giảm trừ</label>
                            <input id="form_reduction_volume" type="number" value="<?=$model->reduction_volume?>" name="reduction_volume"  onchange="change_reduction_material()" autocomplete="off" class="form-control">
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="bmd-label-floating">Khối lượng thực</label>
                            <input id="form_actual_volume" type="number" readOnly = "true" value="<?=$model->actual_volume?>" name="actual_volume"  autocomplete="off" class="form-control">
                        </div>
                    </div>
                    <div id="test" class="col-sm-6 <?= $model->delivery_method?> ">
                    <label class="bmd-label-floating">Phương pháp giao nhận</label>
                    <select class="oneclick_create form-control" name="delivery_id" id="delivery_method" onchange="change_method()">
                    <?php if ($model_method): ?>
                                        <?php foreach ($model_method as $model): ?>
                                            <option value="<?= $model->id ?>"><?= $model->delivery_name?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                            </select>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header card-header-primary text-center">
                                <h6 class="card-title">Cam phía đầu xe</h6>
                            </div>

                            <div class="card-body text-center">
                                <div class="form-group">
                                    <input type="hidden" id="checkin_image_up" name="checkin_image_up" value="<?= $model->checkin_image_up ?>"/>
                                    <img id="img_snap_left" class="image_preview_snap driver_thum" src="<?= $model->checkin_image_up ? $model->checkin_image_up : '/images/empty.jpg' ?>" />

                                </div>
                                <button onclick="snap_video('videoElement_in','img_snap_left','checkin_image_up')" class="btn-capture btn btn-primary btn-round"  type="button">Chụp</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header card-header-info text-center">
                                <h6 class="card-title">Cam ảnh trên thùng</h6>
                            </div>
                            <div class="card-body text-center">
                                <div class="form-group">
                                    <input type="hidden" id="checkin_image_front" name="checkin_image_front" value="<?= $model->checkin_image_front ?>"/>
                                    <img id="img_snap_front" class="image_preview_snap driver_thum"  src="<?= $model->checkin_image_front ? $model->checkin_image_front : '/images/empty.jpg' ?>" />
                                </div>
                                <button onclick="snap_video('videoElement','img_snap_front','checkin_image_front')" class="btn-capture btn btn-info btn-round"  type="button">Chụp</button>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header card-header-primary text-center">
                                <h6 class="card-title">Cam ảnh thùng bên trái</h6>
                            </div>
                            <div class="card-body text-center">
                                <div class="form-group">
                                    <input type="hidden" id="checkin_image_left" name="checkin_image_left" value="<?= $model->checkin_image_left ?>"/>
                                    <img id="img_snap_right" class="image_preview_snap driver_thum"  src="<?= $model->checkin_image_left ? $model->checkin_image_left : '/images/empty.jpg' ?>" />
                                </div>
                                <button onclick="snap_video('videoElement','img_snap_right','checkin_image_left')" class="btn-capture btn btn-primary btn-round"  type="button">Chụp</button>


                            </div>
                        </div>
                    </div>
                     <div class="col-md-6">
                        <div class="card">
                            <div class="card-header card-header-primary text-center">
                                <h6 class="card-title">Cam ảnh thùng bên phải</h6>
                            </div>
                            <div class="card-body text-center">
                                <div class="form-group">
                                    <input type="hidden" id="image_right1" name="image_right1" value="<?= $model->image_right1 ?>"/>
                                    <img id="img_snap_right" class="image_preview_snap driver_thum"  src="<?= $model->image_right1 ? $model->image_right1: '/images/empty.jpg' ?>" />
                                </div>
                                <button onclick="snap_video('videoElement','img_snap_right','image_right')" class="btn-capture btn btn-primary btn-round"  type="button">Chụp</button>


                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <a href="<?= route_to('driver') ?>" class="btn btn-round">Huỷ</a>
                        <button style="margin-left:15px" class="btn btn-success btn-round" type="submit">Lưu</button>
                        <a href="#" onclick="re_print(<?= $model->id ?>);" title="In Phiếu" class="btn btn-success btn-round">In Phiếu</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var is_on_change = false;
    window.onload = function() {

        change_play_cam_gate_in($('#select_camera_in'), 'videoElement_in1');
        change_play_cam_gate_in($('#select_camera_in'), 'videoElement_in2');
        change_play_cam_gate_in($('#select_camera_out'), 'videoElement_in3');
        change_play_cam_gate_out($('#select_camera_out'), 'videoElement_in4');
        // setInterval(refresh_cam, 60*1000);

        document.addEventListener("visibilitychange", visibilitychange);

    };

    function refresh_cam(){
        change_play_cam_gate_in($('#select_camera_in'), 'videoElement_in');
        change_play_cam_gate_out($('#select_camera_out'), 'videoElement_out');
    }

    function visibilitychange (evt) {
        if(is_on_change) return;
        console.log('refresh22222222222222222222222222');
        is_on_change = true;
        refresh_cam();
        is_on_change = false;
    }
</script>




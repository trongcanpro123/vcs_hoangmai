<?php
/**
 * @var \App\Libraries\BaseView $this
 * @var \App\Models\ProjectModel $model
 * @var \CodeIgniter\Validation\Validation $validator
 */
$this->title = 'Quản lý ra vào';
?>
<div class="card">
    <div class="card-header card-header-info flex-align">
        <div>
            <h4 class="card-title"><?= $this->title ?></h4>
        </div>
    </div>
    <div class="card-body">
        <form action="<?= route_to('admin_driver_update', $model->getPrimaryKey()) ?>" method="post"
              enctype="multipart/form-data">

            <?= $this->import('_form_update', ['model' => $model, 'validator' => $validator]) ?>

<!--            <div style="text-align:center;margin-top:25px">-->
<!--                <div class="flex-row" id="add-holder"></div>-->
<!--                <a href="--><?//= route_to('driver') ?><!--" class="btn btn-round">Huỷ</a>-->
<!--                <button style="margin-left:15px;" class="btn btn-info btn-round" type="submit">Lưu</button>-->
<!--            </div>-->
        </form>
    </div>
</div>